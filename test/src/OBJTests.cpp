/*__________________________________________________
 |                                                  |
 |  File: OBJTests.cpp                              |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: OBJ tests.                         |
 |_________________________________________________*/



#include "gtest/gtest.h"
#include "OBJ.h"


TEST(OBJ, EmptyConstructor) {
    obj::OBJ o;

    ASSERT_FALSE(o.isLoaded());

    ASSERT_EQ(o.getVertices().size(), 0);
    ASSERT_EQ(o.getNormals().size(), 0);
    ASSERT_EQ(o.getUVs().size(), 0);
    ASSERT_EQ(o.getFaces().size(), 0);
}

TEST(OBJ, LoadCube) {
    obj::OBJ o("models/cube.obj");

    ASSERT_TRUE(o.isLoaded());

    ASSERT_EQ(o.getVertices().size(), 8);
    ASSERT_EQ(o.getNormals().size(), 6);
    ASSERT_EQ(o.getUVs().size(), 4);
    ASSERT_EQ(o.getFaces().size(), 12);
}

TEST(OBJ, LoadCube_VerticesValues) {
    obj::OBJ o("models/cube.obj");

    ASSERT_TRUE(o.isLoaded());

    EXPECT_EQ(o.getVertices()[0].x, -.5);
    EXPECT_EQ(o.getVertices()[0].y, -.5);
    EXPECT_EQ(o.getVertices()[0].z, .5);

    EXPECT_EQ(o.getVertices()[1].x, .5);
    EXPECT_EQ(o.getVertices()[1].y, -.5);
    EXPECT_EQ(o.getVertices()[1].z, .5);

    EXPECT_EQ(o.getVertices()[2].x, -.5);
    EXPECT_EQ(o.getVertices()[2].y, .5);
    EXPECT_EQ(o.getVertices()[2].z, .5);

    EXPECT_EQ(o.getVertices()[3].x, .5);
    EXPECT_EQ(o.getVertices()[3].y, .5);
    EXPECT_EQ(o.getVertices()[3].z, .5);

    EXPECT_EQ(o.getVertices()[4].x, -.5);
    EXPECT_EQ(o.getVertices()[4].y, .5);
    EXPECT_EQ(o.getVertices()[4].z, -.5);

    EXPECT_EQ(o.getVertices()[5].x, .5);
    EXPECT_EQ(o.getVertices()[5].y, .5);
    EXPECT_EQ(o.getVertices()[5].z, -.5);

    EXPECT_EQ(o.getVertices()[6].x, -.5);
    EXPECT_EQ(o.getVertices()[6].y, -.5);
    EXPECT_EQ(o.getVertices()[6].z, -.5);

    EXPECT_EQ(o.getVertices()[7].x, .5);
    EXPECT_EQ(o.getVertices()[7].y, -.5);
    EXPECT_EQ(o.getVertices()[7].z, -.5);
}

TEST(OBJ, LoadCube_NormalsValues) {
    obj::OBJ o("models/cube.obj");

    ASSERT_TRUE(o.isLoaded());

    EXPECT_EQ(o.getNormals()[0].x, 0.);
    EXPECT_EQ(o.getNormals()[0].y, 0.);
    EXPECT_EQ(o.getNormals()[0].z, 1.);

    EXPECT_EQ(o.getNormals()[1].x, 0.);
    EXPECT_EQ(o.getNormals()[1].y, 1.);
    EXPECT_EQ(o.getNormals()[1].z, 0.);

    EXPECT_EQ(o.getNormals()[2].x, 0.);
    EXPECT_EQ(o.getNormals()[2].y, 0.);
    EXPECT_EQ(o.getNormals()[2].z, -1.);

    EXPECT_EQ(o.getNormals()[3].x, 0.);
    EXPECT_EQ(o.getNormals()[3].y, -1.);
    EXPECT_EQ(o.getNormals()[3].z, 0.);

    EXPECT_EQ(o.getNormals()[4].x, 1.);
    EXPECT_EQ(o.getNormals()[4].y, 0.);
    EXPECT_EQ(o.getNormals()[4].z, 0.);

    EXPECT_EQ(o.getNormals()[5].x, -1.);
    EXPECT_EQ(o.getNormals()[5].y, 0.);
    EXPECT_EQ(o.getNormals()[5].z, 0.);
}

TEST(OBJ, LoadCube_TextureCoordinatesValues) {
    obj::OBJ o("models/cube.obj");

    ASSERT_TRUE(o.isLoaded());

    EXPECT_EQ(o.getUVs()[0].x, 0.);
    EXPECT_EQ(o.getUVs()[0].y, 0.);

    EXPECT_EQ(o.getUVs()[1].x, 1.);
    EXPECT_EQ(o.getUVs()[1].y, 0.);

    EXPECT_EQ(o.getUVs()[2].x, 0.);
    EXPECT_EQ(o.getUVs()[2].y, 1.);

    EXPECT_EQ(o.getUVs()[3].x, 1.);
    EXPECT_EQ(o.getUVs()[3].y, 1.);
}

TEST(OBJ, LoadCube_FacesValues) {
    obj::OBJ o("models/cube.obj");

    ASSERT_TRUE(o.isLoaded());

    obj::Face f = o.getFaces()[0];
    EXPECT_EQ(f.v0, 1 - 1);
    EXPECT_EQ(f.v1, 2 - 1);
    EXPECT_EQ(f.v2, 3 - 1);
    EXPECT_EQ(f.tc0, 1 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 3 - 1);
    EXPECT_EQ(f.n0, 1 - 1);
    EXPECT_EQ(f.n1, 1 - 1);
    EXPECT_EQ(f.n2, 1 - 1);

    f = o.getFaces()[1];
    EXPECT_EQ(f.v0, 3 - 1);
    EXPECT_EQ(f.v1, 2 - 1);
    EXPECT_EQ(f.v2, 4 - 1);
    EXPECT_EQ(f.tc0, 3 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 4 - 1);
    EXPECT_EQ(f.n0, 1 - 1);
    EXPECT_EQ(f.n1, 1 - 1);
    EXPECT_EQ(f.n2, 1 - 1);

    f = o.getFaces()[2];
    EXPECT_EQ(f.v0, 3 - 1);
    EXPECT_EQ(f.v1, 4 - 1);
    EXPECT_EQ(f.v2, 5 - 1);
    EXPECT_EQ(f.tc0, 1 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 3 - 1);
    EXPECT_EQ(f.n0, 2 - 1);
    EXPECT_EQ(f.n1, 2 - 1);
    EXPECT_EQ(f.n2, 2 - 1);

    f = o.getFaces()[3];
    EXPECT_EQ(f.v0, 5 - 1);
    EXPECT_EQ(f.v1, 4 - 1);
    EXPECT_EQ(f.v2, 6 - 1);
    EXPECT_EQ(f.tc0, 3 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 4 - 1);
    EXPECT_EQ(f.n0, 2 - 1);
    EXPECT_EQ(f.n1, 2 - 1);
    EXPECT_EQ(f.n2, 2 - 1);

    f = o.getFaces()[4];
    EXPECT_EQ(f.v0, 5 - 1);
    EXPECT_EQ(f.v1, 6 - 1);
    EXPECT_EQ(f.v2, 7 - 1);
    EXPECT_EQ(f.tc0, 4 - 1);
    EXPECT_EQ(f.tc1, 3 - 1);
    EXPECT_EQ(f.tc2, 2 - 1);
    EXPECT_EQ(f.n0, 3 - 1);
    EXPECT_EQ(f.n1, 3 - 1);
    EXPECT_EQ(f.n2, 3 - 1);

    f = o.getFaces()[5];
    EXPECT_EQ(f.v0, 7 - 1);
    EXPECT_EQ(f.v1, 6 - 1);
    EXPECT_EQ(f.v2, 8 - 1);
    EXPECT_EQ(f.tc0, 2 - 1);
    EXPECT_EQ(f.tc1, 3 - 1);
    EXPECT_EQ(f.tc2, 1 - 1);
    EXPECT_EQ(f.n0, 3 - 1);
    EXPECT_EQ(f.n1, 3 - 1);
    EXPECT_EQ(f.n2, 3 - 1);

    f = o.getFaces()[6];
    EXPECT_EQ(f.v0, 7 - 1);
    EXPECT_EQ(f.v1, 8 - 1);
    EXPECT_EQ(f.v2, 1 - 1);
    EXPECT_EQ(f.tc0, 1 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 3 - 1);
    EXPECT_EQ(f.n0, 4 - 1);
    EXPECT_EQ(f.n1, 4 - 1);
    EXPECT_EQ(f.n2, 4 - 1);

    f = o.getFaces()[7];
    EXPECT_EQ(f.v0, 1 - 1);
    EXPECT_EQ(f.v1, 8 - 1);
    EXPECT_EQ(f.v2, 2 - 1);
    EXPECT_EQ(f.tc0, 3 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 4 - 1);
    EXPECT_EQ(f.n0, 4 - 1);
    EXPECT_EQ(f.n1, 4 - 1);
    EXPECT_EQ(f.n2, 4 - 1);

    f = o.getFaces()[8];
    EXPECT_EQ(f.v0, 2 - 1);
    EXPECT_EQ(f.v1, 8 - 1);
    EXPECT_EQ(f.v2, 4 - 1);
    EXPECT_EQ(f.tc0, 1 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 3 - 1);
    EXPECT_EQ(f.n0, 5 - 1);
    EXPECT_EQ(f.n1, 5 - 1);
    EXPECT_EQ(f.n2, 5 - 1);

    f = o.getFaces()[9];
    EXPECT_EQ(f.v0, 4 - 1);
    EXPECT_EQ(f.v1, 8 - 1);
    EXPECT_EQ(f.v2, 6 - 1);
    EXPECT_EQ(f.tc0, 3 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 4 - 1);
    EXPECT_EQ(f.n0, 5 - 1);
    EXPECT_EQ(f.n1, 5 - 1);
    EXPECT_EQ(f.n2, 5 - 1);

    f = o.getFaces()[10];
    EXPECT_EQ(f.v0, 7 - 1);
    EXPECT_EQ(f.v1, 1 - 1);
    EXPECT_EQ(f.v2, 5 - 1);
    EXPECT_EQ(f.tc0, 1 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 3 - 1);
    EXPECT_EQ(f.n0, 6 - 1);
    EXPECT_EQ(f.n1, 6 - 1);
    EXPECT_EQ(f.n2, 6 - 1);

    f = o.getFaces()[11];
    EXPECT_EQ(f.v0, 5 - 1);
    EXPECT_EQ(f.v1, 1 - 1);
    EXPECT_EQ(f.v2, 3 - 1);
    EXPECT_EQ(f.tc0, 3 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 4 - 1);
    EXPECT_EQ(f.n0, 6 - 1);
    EXPECT_EQ(f.n1, 6 - 1);
    EXPECT_EQ(f.n2, 6 - 1);
}


TEST(OBJ, LoadCube_AddVertex) {
    obj::OBJ o("models/cube.obj");

    ASSERT_TRUE(o.isLoaded());

    unsigned int index = o.addVertex(math::Vector3(1, 1, 1));

    EXPECT_EQ(index, 8);
    EXPECT_EQ(o.getVertices().size(), 9);
    EXPECT_EQ(o.getVertices()[8].x, 1.);
    EXPECT_EQ(o.getVertices()[8].y, 1.);
    EXPECT_EQ(o.getVertices()[8].z, 1.);
}


TEST(OBJ, LoadCube_UpdateFaces) {
    obj::OBJ o("models/cube.obj");

    ASSERT_TRUE(o.isLoaded());

    ASSERT_EQ(o.getFaces().size(), 12);
    std::vector<obj::Face> faces = o.getFaces();
    faces.pop_back();
    o.updateFaces(faces);
    ASSERT_EQ(o.getFaces().size(), 11);
}


TEST(OBJ, LoadCube_GetIndices) {
    obj::OBJ o("models/cube.obj");

    ASSERT_TRUE(o.isLoaded());

    ASSERT_EQ(o.getIndices().size(), 36);
}


TEST(OBJ, Save) {

    obj::OBJ o("models/cube.obj");
    ASSERT_TRUE(o.isLoaded());

    ASSERT_EQ(o.getVertices().size(), 8);
    ASSERT_EQ(o.getNormals().size(), 6);
    ASSERT_EQ(o.getUVs().size(), 4);
    ASSERT_EQ(o.getFaces().size(), 12);


    o.saveModel("models/cube_copy.obj");
    obj::OBJ copy("models/cube_copy.obj");
    ASSERT_TRUE(copy.isLoaded());

    ASSERT_EQ(copy.getVertices().size(), 8);
    ASSERT_EQ(copy.getNormals().size(), 6);
    ASSERT_EQ(copy.getUVs().size(), 4);
    ASSERT_EQ(copy.getFaces().size(), 12);
}


TEST(OBJ, Save_VerticesValues) {
    obj::OBJ original("models/cube.obj");
    ASSERT_TRUE(original.isLoaded());
    original.saveModel("models/cube_copy.obj");
    obj::OBJ o("models/cube_copy.obj");
    ASSERT_TRUE(o.isLoaded());

    EXPECT_EQ(o.getVertices()[0].x, -.5);
    EXPECT_EQ(o.getVertices()[0].y, -.5);
    EXPECT_EQ(o.getVertices()[0].z, .5);

    EXPECT_EQ(o.getVertices()[1].x, .5);
    EXPECT_EQ(o.getVertices()[1].y, -.5);
    EXPECT_EQ(o.getVertices()[1].z, .5);

    EXPECT_EQ(o.getVertices()[2].x, -.5);
    EXPECT_EQ(o.getVertices()[2].y, .5);
    EXPECT_EQ(o.getVertices()[2].z, .5);

    EXPECT_EQ(o.getVertices()[3].x, .5);
    EXPECT_EQ(o.getVertices()[3].y, .5);
    EXPECT_EQ(o.getVertices()[3].z, .5);

    EXPECT_EQ(o.getVertices()[4].x, -.5);
    EXPECT_EQ(o.getVertices()[4].y, .5);
    EXPECT_EQ(o.getVertices()[4].z, -.5);

    EXPECT_EQ(o.getVertices()[5].x, .5);
    EXPECT_EQ(o.getVertices()[5].y, .5);
    EXPECT_EQ(o.getVertices()[5].z, -.5);

    EXPECT_EQ(o.getVertices()[6].x, -.5);
    EXPECT_EQ(o.getVertices()[6].y, -.5);
    EXPECT_EQ(o.getVertices()[6].z, -.5);

    EXPECT_EQ(o.getVertices()[7].x, .5);
    EXPECT_EQ(o.getVertices()[7].y, -.5);
    EXPECT_EQ(o.getVertices()[7].z, -.5);
}

TEST(OBJ, Save_NormalsValues) {
    obj::OBJ original("models/cube.obj");
    ASSERT_TRUE(original.isLoaded());
    original.saveModel("models/cube_copy.obj");
    obj::OBJ o("models/cube_copy.obj");
    ASSERT_TRUE(o.isLoaded());

    EXPECT_EQ(o.getNormals()[0].x, 0.);
    EXPECT_EQ(o.getNormals()[0].y, 0.);
    EXPECT_EQ(o.getNormals()[0].z, 1.);

    EXPECT_EQ(o.getNormals()[1].x, 0.);
    EXPECT_EQ(o.getNormals()[1].y, 1.);
    EXPECT_EQ(o.getNormals()[1].z, 0.);

    EXPECT_EQ(o.getNormals()[2].x, 0.);
    EXPECT_EQ(o.getNormals()[2].y, 0.);
    EXPECT_EQ(o.getNormals()[2].z, -1.);

    EXPECT_EQ(o.getNormals()[3].x, 0.);
    EXPECT_EQ(o.getNormals()[3].y, -1.);
    EXPECT_EQ(o.getNormals()[3].z, 0.);

    EXPECT_EQ(o.getNormals()[4].x, 1.);
    EXPECT_EQ(o.getNormals()[4].y, 0.);
    EXPECT_EQ(o.getNormals()[4].z, 0.);

    EXPECT_EQ(o.getNormals()[5].x, -1.);
    EXPECT_EQ(o.getNormals()[5].y, 0.);
    EXPECT_EQ(o.getNormals()[5].z, 0.);
}

TEST(OBJ, Save_TextureCoordinatesValues) {
    obj::OBJ original("models/cube.obj");
    ASSERT_TRUE(original.isLoaded());
    original.saveModel("models/cube_copy.obj");
    obj::OBJ o("models/cube_copy.obj");
    ASSERT_TRUE(o.isLoaded());

    EXPECT_EQ(o.getUVs()[0].x, 0.);
    EXPECT_EQ(o.getUVs()[0].y, 0.);

    EXPECT_EQ(o.getUVs()[1].x, 1.);
    EXPECT_EQ(o.getUVs()[1].y, 0.);

    EXPECT_EQ(o.getUVs()[2].x, 0.);
    EXPECT_EQ(o.getUVs()[2].y, 1.);

    EXPECT_EQ(o.getUVs()[3].x, 1.);
    EXPECT_EQ(o.getUVs()[3].y, 1.);
}

TEST(OBJ, Save_FacesValues) {
    obj::OBJ original("models/cube.obj");
    ASSERT_TRUE(original.isLoaded());
    original.saveModel("models/cube_copy.obj");
    obj::OBJ o("models/cube_copy.obj");
    ASSERT_TRUE(o.isLoaded());

    obj::Face f = o.getFaces()[0];
    EXPECT_EQ(f.v0, 1 - 1);
    EXPECT_EQ(f.v1, 2 - 1);
    EXPECT_EQ(f.v2, 3 - 1);
    EXPECT_EQ(f.tc0, 1 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 3 - 1);
    EXPECT_EQ(f.n0, 1 - 1);
    EXPECT_EQ(f.n1, 1 - 1);
    EXPECT_EQ(f.n2, 1 - 1);

    f = o.getFaces()[1];
    EXPECT_EQ(f.v0, 3 - 1);
    EXPECT_EQ(f.v1, 2 - 1);
    EXPECT_EQ(f.v2, 4 - 1);
    EXPECT_EQ(f.tc0, 3 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 4 - 1);
    EXPECT_EQ(f.n0, 1 - 1);
    EXPECT_EQ(f.n1, 1 - 1);
    EXPECT_EQ(f.n2, 1 - 1);

    f = o.getFaces()[2];
    EXPECT_EQ(f.v0, 3 - 1);
    EXPECT_EQ(f.v1, 4 - 1);
    EXPECT_EQ(f.v2, 5 - 1);
    EXPECT_EQ(f.tc0, 1 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 3 - 1);
    EXPECT_EQ(f.n0, 2 - 1);
    EXPECT_EQ(f.n1, 2 - 1);
    EXPECT_EQ(f.n2, 2 - 1);

    f = o.getFaces()[3];
    EXPECT_EQ(f.v0, 5 - 1);
    EXPECT_EQ(f.v1, 4 - 1);
    EXPECT_EQ(f.v2, 6 - 1);
    EXPECT_EQ(f.tc0, 3 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 4 - 1);
    EXPECT_EQ(f.n0, 2 - 1);
    EXPECT_EQ(f.n1, 2 - 1);
    EXPECT_EQ(f.n2, 2 - 1);

    f = o.getFaces()[4];
    EXPECT_EQ(f.v0, 5 - 1);
    EXPECT_EQ(f.v1, 6 - 1);
    EXPECT_EQ(f.v2, 7 - 1);
    EXPECT_EQ(f.tc0, 4 - 1);
    EXPECT_EQ(f.tc1, 3 - 1);
    EXPECT_EQ(f.tc2, 2 - 1);
    EXPECT_EQ(f.n0, 3 - 1);
    EXPECT_EQ(f.n1, 3 - 1);
    EXPECT_EQ(f.n2, 3 - 1);

    f = o.getFaces()[5];
    EXPECT_EQ(f.v0, 7 - 1);
    EXPECT_EQ(f.v1, 6 - 1);
    EXPECT_EQ(f.v2, 8 - 1);
    EXPECT_EQ(f.tc0, 2 - 1);
    EXPECT_EQ(f.tc1, 3 - 1);
    EXPECT_EQ(f.tc2, 1 - 1);
    EXPECT_EQ(f.n0, 3 - 1);
    EXPECT_EQ(f.n1, 3 - 1);
    EXPECT_EQ(f.n2, 3 - 1);

    f = o.getFaces()[6];
    EXPECT_EQ(f.v0, 7 - 1);
    EXPECT_EQ(f.v1, 8 - 1);
    EXPECT_EQ(f.v2, 1 - 1);
    EXPECT_EQ(f.tc0, 1 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 3 - 1);
    EXPECT_EQ(f.n0, 4 - 1);
    EXPECT_EQ(f.n1, 4 - 1);
    EXPECT_EQ(f.n2, 4 - 1);

    f = o.getFaces()[7];
    EXPECT_EQ(f.v0, 1 - 1);
    EXPECT_EQ(f.v1, 8 - 1);
    EXPECT_EQ(f.v2, 2 - 1);
    EXPECT_EQ(f.tc0, 3 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 4 - 1);
    EXPECT_EQ(f.n0, 4 - 1);
    EXPECT_EQ(f.n1, 4 - 1);
    EXPECT_EQ(f.n2, 4 - 1);

    f = o.getFaces()[8];
    EXPECT_EQ(f.v0, 2 - 1);
    EXPECT_EQ(f.v1, 8 - 1);
    EXPECT_EQ(f.v2, 4 - 1);
    EXPECT_EQ(f.tc0, 1 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 3 - 1);
    EXPECT_EQ(f.n0, 5 - 1);
    EXPECT_EQ(f.n1, 5 - 1);
    EXPECT_EQ(f.n2, 5 - 1);

    f = o.getFaces()[9];
    EXPECT_EQ(f.v0, 4 - 1);
    EXPECT_EQ(f.v1, 8 - 1);
    EXPECT_EQ(f.v2, 6 - 1);
    EXPECT_EQ(f.tc0, 3 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 4 - 1);
    EXPECT_EQ(f.n0, 5 - 1);
    EXPECT_EQ(f.n1, 5 - 1);
    EXPECT_EQ(f.n2, 5 - 1);

    f = o.getFaces()[10];
    EXPECT_EQ(f.v0, 7 - 1);
    EXPECT_EQ(f.v1, 1 - 1);
    EXPECT_EQ(f.v2, 5 - 1);
    EXPECT_EQ(f.tc0, 1 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 3 - 1);
    EXPECT_EQ(f.n0, 6 - 1);
    EXPECT_EQ(f.n1, 6 - 1);
    EXPECT_EQ(f.n2, 6 - 1);

    f = o.getFaces()[11];
    EXPECT_EQ(f.v0, 5 - 1);
    EXPECT_EQ(f.v1, 1 - 1);
    EXPECT_EQ(f.v2, 3 - 1);
    EXPECT_EQ(f.tc0, 3 - 1);
    EXPECT_EQ(f.tc1, 2 - 1);
    EXPECT_EQ(f.tc2, 4 - 1);
    EXPECT_EQ(f.n0, 6 - 1);
    EXPECT_EQ(f.n1, 6 - 1);
    EXPECT_EQ(f.n2, 6 - 1);
}
git submodule update --init
git submodule update --remote

rmdir /s /Q build
mkdir build
cd build
cmake .. -G "MinGW Makefiles"
cmake --build . --config Release
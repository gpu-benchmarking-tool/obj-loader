git submodule update --init --recursive
git submodule update --remote --recursive

rm build -rf
mkdir build
cd build
cmake ..
make -j 12
make install | egrep -v 'gmock|gtest'

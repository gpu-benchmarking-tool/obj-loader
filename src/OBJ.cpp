/*__________________________________________________
 |                                                  |
 |  File: OBJ.cpp                                   |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: Loads an OBJ file.                 |
 |_________________________________________________*/



#include "OBJ.h"

using namespace obj;


// Empty constructor.
OBJ::OBJ() {
    clear();
}


// Constructor.
OBJ::OBJ(const char *modelPath) {
    clear();
    loadModel(modelPath);
}


// Loads a model.
void OBJ::loadModel(const char *filename) {

    // Clear OBJ
    clear();

    // Open the file
    std::ifstream file(filename);
    std::string line;

    if (!file.is_open()) return;

    // Read the file line by line
    while (std::getline(file, line)) {

        // Get the first word
        std::string word;
        std::stringstream ss(line);
        ss >> word;

        // "v" is a vertex
        if (word == "v") {
            // Extract the data and add it to the vector
            float x, y, z;
            ss >> x >> y >> z;
            vertices.push_back(math::Vector3(x, y, z));
        }

        // "vt" is a texture coordinate
        else if (word == "vt") {
            // Extract the data and add it to the vector
            float x, y;
            ss >> x >> y;
            textureCoordinates.push_back(math::Vector2(x, y));
        }

        // "vn" is a normal
        else if (word == "vn") {
            // Extract the data and add it to the vector
            float x, y, z;
            ss >> x >> y >> z;
            normals.push_back(math::Vector3(x, y, z));
        }

        // "f" is a face
        else if (word == "f") {

            uint32_t v0, v1, v2;
            uint32_t tc0, tc1, tc2;
            uint32_t n0, n1, n2;

            // Extract the first vertex data
            ss >> word;
            std::replace(word.begin(), word.end(), '/', ' ');
            std::stringstream ss1(word);
            ss1 >> v0 >> tc0 >> n0;

            // Extract the second vertex data
            ss >> word;
            std::replace(word.begin(), word.end(), '/', ' ');
            std::stringstream ss2(word);
            ss2 >> v1 >> tc1 >> n1;

            // Extract the third vertex data
            ss >> word;
            std::replace(word.begin(), word.end(), '/', ' ');
            std::stringstream ss3(word);
            ss3 >> v2 >> tc2 >> n2;

            // Add the face to the vector
            faces.push_back(Face(v0 - 1, tc0 - 1, n0 - 1, v1 - 1, tc1 - 1, n1 - 1, v2 - 1, tc2 - 1, n2 - 1));
        }
    }

    // Close the file
    file.close();
    loaded = true;
}


// Saves a model.
void OBJ::saveModel(const char *filename) {

    // Open the file
    std::ofstream file(filename);

    if (!file.is_open()) return;

    // Write  vertices
    for (unsigned i = 0; i < vertices.size(); ++i)
        file << "v " << vertices[i].x << " " << vertices[i].y << " " << vertices[i].z << std::endl;

    // Write texture coordinates
    for (unsigned i = 0; i < textureCoordinates.size(); ++i)
        file << "vt " << textureCoordinates[i].x << " " << textureCoordinates[i].y << std::endl;

    // Write normals
    for (unsigned i = 0; i < normals.size(); ++i)
        file << "vn " << normals[i].x << " " << normals[i].y << " " << normals[i].z << std::endl;

    // Write faces
    for (unsigned i = 0; i < faces.size(); ++i)
        file << "f "
             << faces[i].v0 + 1 << "/" << faces[i].tc0 + 1 << "/" << faces[i].n0 + 1 << " "
             << faces[i].v1 + 1 << "/" << faces[i].tc1 + 1 << "/" << faces[i].n1 + 1 << " "
             << faces[i].v2 + 1 << "/" << faces[i].tc2 + 1 << "/" << faces[i].n2 + 1 << std::endl;


    // Close the file
    file.close();
}


// Adds a vertex.
unsigned int OBJ::addVertex(math::Vector3 vertex) {
    vertices.push_back(vertex);
    return vertices.size() - 1;
}


// Gets the indices.
std::vector<unsigned> OBJ::getIndices() {

    std::vector<unsigned> indices;

    for (unsigned i = 0; i < faces.size(); ++i) {
        indices.push_back(faces[i].v0);
        indices.push_back(faces[i].v1);
        indices.push_back(faces[i].v2);
    }

    return indices;
}


// Clears the data.
void OBJ::clear() {
    loaded = false;
    vertices.clear();
    textureCoordinates.clear();
    normals.clear();
    faces.clear();
}

/*__________________________________________________
 |                                                  |
 |  File: Face.hpp                                  |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: Represents one face.               |
 |_________________________________________________*/



#pragma once
#pragma GCC diagnostic ignored "-Wreorder"

namespace obj {

/**
 * @brief      Class used to represent a face.
 * A face is represented by 3 points. Each point is composed
 * of a position, a texture coordinate and a normal.
 * The data is not saved here but only the index.
 */
class Face {

  public:

    unsigned v0, v1, v2;    // Vertices indices
    unsigned tc0, tc1, tc2; // Texture coordinates indices
    unsigned n0, n1, n2;    // Normals indices


    /**
     * @brief      Constructs the object.
     * Empty constructor sets vertices, texture coordinates and normals indices to 0.
     */
    Face() : v0(0), tc0(0), n0(0), v1(0), tc1(0), n1(0), v2(0), tc2(0), n2(0) {}


    /**
     * @brief      Constructs the object.
     *
     * @param[in]  v0    First vertex index
     * @param[in]  tc0   First vertex texture coordinate index
     * @param[in]  n0    First vertex normal index
     * @param[in]  v1    First vertex index
     * @param[in]  tc1   First vertex texture coordinate index
     * @param[in]  n1    First vertex normal index
     * @param[in]  v2    First vertex index
     * @param[in]  tc2   First vertex texture coordinate index
     * @param[in]  n2    First vertex normal index
     */
    Face(unsigned v0, unsigned tc0, unsigned n0, unsigned v1, unsigned tc1, unsigned n1, unsigned v2, unsigned tc2, unsigned n2) :
        v0(v0), tc0(tc0), n0(n0), v1(v1), tc1(tc1), n1(n1), v2(v2), tc2(tc2), n2(n2) {}
};

}

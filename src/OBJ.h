/*__________________________________________________
 |                                                  |
 |  File: OBJ.cpp                                   |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: Loads an OBJ file.                 |
 |_________________________________________________*/



#pragma once

#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include "MathLibrary.hpp"
#include "Face.hpp"

namespace obj {



/**
 * @brief      Class for OBJ.
 * Loads and OBJ loader form file, saves a model to path,
 * adds a vertex, updates the faces and gets the data.
 * The data is saved as vectors in an std::vector container
 * id is accessed by index.
 */
class OBJ {

  private:
    bool loaded;
    std::vector<math::Vector3> vertices, normals;
    std::vector<math::Vector2> textureCoordinates;
    std::vector<Face> faces;

  public:

    /**
     * @brief      Constructs the object.
     * Empty constructor.
     */
    OBJ();


    /**
     * @brief      Constructs the object.
     * Loads a 3D model from path.
     *
     * @param[in]  modelPath    The model path to load
     */
    OBJ(const char *modelPath);


    /**
     * @brief      Destroys the object.
     */
    ~OBJ() { clear(); }


    /**
     * @brief      Loads a model.
     * Loads a 3D model given a path.
     *
     * @param[in]  filename  The filename
     */
    void loadModel(const char *filename);


    /**
     * @brief      Saves a model.
     * Saves the OBJ to disk given a path.
     *
     * @param[in]  filename  The filename
     */
    void saveModel(const char *filename);


    /**
     * @brief      Determines if loaded.
     * Checks if the OBJ is successfully loaded.
     *
     * @return     True if loaded, False otherwise.
     */
    bool isLoaded() { return loaded; }


    /**
     * @brief      Gets the vertices.
     * Fetches the list of vertices in the OBJ.
     *
     * @return     The vertices list.
     */
    std::vector<math::Vector3> getVertices() { return vertices; }


    /**
     * @brief      Gets the normals.
     * Fetches the list of normals in the OBJ.
     *
     * @return     The normals list.
     */
    std::vector<math::Vector3> getNormals() { return normals; }


    /**
     * @brief      Gets the texture coordinates.
     * Fetches the list of UV coordinates in the OBJ.
     *
     * @return     The texture coordinates list.
     */
    std::vector<math::Vector2> getUVs() { return textureCoordinates; }


    /**
     * @brief      Gets the faces.
     * Fetches the list of faces in the OBJ.
     *
     * @return     The faces list.
     */
    std::vector<Face> getFaces() { return faces; }


    /**
     * @brief      Gets the indices.
     * Fetches the list of indices in the OBJ.
     *
     * @return     The indices list.
     */
    std::vector<unsigned> getIndices();


    /**
     * @brief      Adds a vertex.
     * Adds a vertex to the OBJ.
     *
     * @param[in]  vertex  The vertex
     *
     * @return     Vertex index.
     */
    unsigned int addVertex(math::Vector3 vertex);


    /**
     * @brief      Updates the faces.
     * Updates the list of faces.
     *
     * @param[in]  newFaces  The new faces
     */
    void updateFaces(std::vector<Face> newFaces) { faces = newFaces; }


  private:

    /**
     * @brief      Clears the data.
     */
    void clear();

};
}